﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignedIn.aspx.cs" Inherits="Faizi.SignedIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Portfolio | Bootstrap .net Templates</title>
    <%-- ------ CSS ------ --%>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/favicon.ico" />
</head>
<body>
    <form id="form2" runat="server">
        <header id="header">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="top-number">
                                <p><i class="fa fa-thumbs-up"></i>Keep In Tounch </p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="social">
                                <ul class="social-share">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <%--container--%>
            </div>
            <%--/top-bar--%>
            <nav class="navbar navbar-inverse" role="banner">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx">
                            <img src="images/logo.png" alt="logo"></a>
                    </div>
                        
                    <div class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="active"><asp:Label ID="UName" ForeColor="White" runat="server" Text=""></asp:Label></li>
                            <li><a href="Default.aspx">Sign Out</a></li>
                        </ul>
                    </div>
                </div>
                <!--/.container-->
            </nav>
            <!--/nav-->

        </header>
        <section id="portfolio">
            <div class="container">
                <div class="center">
                    <h2><asp:Label ID="MainHead" Text="" runat="server"></asp:Label></h2>
                    <p class="lead"><asp:Label ID="ScndDesc" Text="Put your images online to share with your family easily!" runat="server"></asp:Label> 
                        <br>
                        <asp:Label ID="FinalDesc" Text="Create an Album to add an image. Already created one? Browse through albums to deal with your family photos." runat="server"></asp:Label></p>
                </div>

                <!--/#portfolio-filter-->
                
                <div class="row">
                    <asp:Panel ID="MainMenu" runat="server">
                        <asp:Button ID="CA" Text="Create Album" BackColor="Blue" ForeColor="White" CssClass="btn btn-slide" runat="server" OnClick="CA_Click"></asp:Button>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:Button ID="VA" ForeColor="White" BackColor="Blue" CssClass="btn btn-slide" Text="View Albums" runat="server" OnClick="VA_Click" />
                    </asp:Panel>
                </div>
            </div>
            <div class="row">
                <asp:Panel ID="SelAlPan" runat="server" Visible="false">
                    <asp:DropDownList ID="Albums" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                    </asp:DropDownList>
                    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="GoView" runat="server" BackColor="Blue" CssClass="btn btn-slide" ForeColor="White" OnClick="GoView_Click" Text="Go!" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Cancel" runat="server" Text="Cancel" ForeColor="White" BackColor="Blue" CssClass="btn btn-slide" OnClick="Cancel_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="DelAl" runat="server" Text="Delete Album" ForeColor="White" BackColor="Blue" CssClass="btn btn-slide" OnClick="DelAl_Click" />
                </asp:Panel>
            </div>

            <div class="row">
                <asp:Panel ID="CrAlPan" runat="server" Visible="false">
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:TextBox ID="AlbumName" runat="server" CssClass="form-control" placeholder="Album Title..."></asp:TextBox>
                    <br />
                    <asp:Button ID="Create" runat="server" BackColor="Blue" ForeColor="White" CssClass="btn btn-slide" OnClick="Create_Click" Text="Create!" />
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:Button ID="CancelCr" runat="server" BackColor="Blue" ForeColor="White" CssClass="btn btn-slide" OnClick="CancelCr_Click" Text="Cancel" />
                </asp:Panel>
            </div>
        </section>

        <!-- Back To Top -->
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var offset = 300;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.back-to-top').fadeIn(duration);
                    } else {
                        jQuery('.back-to-top').fadeOut(duration);
                    }
                });

                jQuery('.back-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({ scrollTop: 0 }, duration);
                    return false;
                })
            });
        </script>
        <!-- /top-link-block -->
        <!-- Jscript -->
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
        <script src="js/jquery.isotope.min.js" type="text/javascript"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/wow.min.js" type="text/javascript"></script>
    </form>
</body>
</html>

