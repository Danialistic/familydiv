﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Faizi.DAL;

namespace Faizi
{
    public partial class Login : System.Web.UI.Page
    {
        private myDAL Connectifier = new myDAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            int Stat=-32;
            int UserID = 0;
            Connectifier.Login(this.txtName.Text.ToString(),this.txtsubject.Text.ToString(),ref Stat, ref UserID);

            if(Stat==-32)
            {
                this.RespnseHandlerLabel.Text="Cannot connect to database at the moment!";
            }
            else if(Stat==1)
            {
                this.RespnseHandlerLabel.Text = "Invalid Password!";
            }
            else if(Stat==2)
            {
                this.RespnseHandlerLabel.Text = "This email is not yet registered on our servers yet!";
            }
            else if(Stat==3)
            {
                this.RespnseHandlerLabel.Text = "Welcome to FamilyDiv";
                Session["LoggedInUserID"] = UserID.ToString();
                Response.Redirect("SignedIn.aspx");
            }
            else if(Stat==4)
            {
                Session["ActCode"] = UserID.ToString();
                Response.Redirect("ActivationPending.aspx");
            }
        }
    }
}