﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Faizi.DAL;

namespace Faizi
{
    public partial class ActivationPending : System.Web.UI.Page
    {
        myDAL Client = new myDAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            //agar wo sai hai to us ka data unactivated users k table se nikalo or activated main dalo or usay main page pe redirect kr dou
            if(this.Code.Text.ToString()==Session["ActCode"].ToString())
            {
                Client.ActivateUser(Session["ActCode"].ToString());
                Response.Redirect("Default.aspx");
            }
            else
            {
                this.RespnseHandlerLabel.Text = "Invalid activation code!";
            }
        }
    }
}