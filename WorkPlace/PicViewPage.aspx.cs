﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Faizi.DAL;

namespace Faizi
{
    public partial class PicViewPage : System.Web.UI.Page
    {
        myDAL DBConnection = new myDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.DrpDnLstForPics.Visible = false;
            DataSet Pics=new DataSet();
            DBConnection.GetAlbumPics(Session["SelAlbumID"].ToString(), ref Pics);
            this.MainHead.Text = DBConnection.GetAlbumName(Convert.ToInt32(Session["SelAlbumID"].ToString()));
            this.MainHead.Text += "'s Pictures.";
            for(int i=0;i<Pics.Tables[0].Rows.Count;i++)
            {
                ConstructDiv(Pics.Tables[0].Rows[i]["PicTitle"].ToString(), "Added by " + DBConnection.GetUserName(Convert.ToInt32(Pics.Tables[0].Rows[i]["AddedBy"].ToString()))+ " On " + Pics.Tables[0].Rows[i]["PicDate"].ToString(), "Data/" + Pics.Tables[0].Rows[i]["PicTitle"].ToString());
            }
        }

        private void ConstructDiv(string TtlDesc, string DescDet, string thmb)
        {
            string disp = "\"";
            disp += thmb;
            disp += "\"";
            System.Web.UI.HtmlControls.HtmlGenericControl DivMaker;
            
            DivMaker = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            DivMaker.ID = "createDiv";
            DivMaker.InnerHtml = "<div class=\"portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3\"> <div class=\"recent-work-wrap\"> <img class=\"img-responsive\" src= " + disp + "alt=\"Unable to display image due to some error!\" height=\"500px\" > <div class=\"overlay\"> <div class=\"recent-work-inner\">    <h3><a href=\"#\">" + TtlDesc + "</a></h3> <p>" + DescDet + "</p> <a class=\"preview\" href=" + disp + " rel=\"prettyPhoto\"><i class=\"fa fa-eye\"></i>View</a> </div>  </div> </div> </div>";
            this.MovieDisplayArea.Controls.Add(DivMaker);

        }

        protected void UpldImg_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadImage.aspx");
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            Response.Redirect("SignedIn.aspx");
        }

        protected void DelImg_Click(object sender, EventArgs e)
        {
            this.DrpDnLstForPics.Visible = true;
            DBConnection.PopulatePicsList(Session["SelAlbumID"].ToString(), ref this.PicsList);
        }

        protected void FinDel_Click(object sender, EventArgs e)
        {
            if (this.PicsList.SelectedValue != "0")
            {
                DBConnection.DelPic(this.PicsList.SelectedValue.ToString());
                System.IO.File.Delete("/Data/" + this.PicsList.SelectedItem.Text.ToString());
                Response.Redirect("PicViewPage.aspx");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Select a picture first!')", true);
            }
        }
    }
}