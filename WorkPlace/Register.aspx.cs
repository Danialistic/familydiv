﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Faizi.DAL;
using System.Net.Mail;

namespace Faizi
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        myDAL Connect=new myDAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if(this.Pass.Text.ToString()==this.Repass.Text.ToString())
            {
                int ActNo = 0;
                ActNo = Connect.SignMeUp(this.Fname.Text.ToString(), this.EmailID.Text.ToString(), this.Pass.Text.ToString(), this.ContactNo.Text.ToString());
                this.ResponseHandlerLabel.Text = "You have been registered successfully. Please email the admin (danialves.chelsea@gmail.com) for activation code!";
            }
            else
            {
                this.ResponseHandlerLabel.Text = "Passwords dont match!";
            }
        }
    }
}