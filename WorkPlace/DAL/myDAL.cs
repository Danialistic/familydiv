﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Net.Mail;

namespace Faizi.DAL
{

    public class myDAL
    {
        private static readonly string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
       
        public void SelectItem() 
        {
            SqlConnection con = new SqlConnection(connString); //declare and instantiate new SQL connection
            try
            {
                con.Open(); // open sql Connection
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("SELECT * FROM Users", con);  //instantiate SQL command 
                //cmd.CommandType = CommandType.Text; //set type of sqL Command
                cmd.ExecuteReader();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }

        }

        public void Login(string email, string pass, ref int status, ref int  UserId)
        {
            int Found = 0;
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            UserId = -32;
            try
            {
                SqlCommand cmnd = new SqlCommand("UserLogin ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@Email", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Pass", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Temp", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@UID", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmnd.Parameters.Add("@Status", SqlDbType.Int).Direction = ParameterDirection.Output;

                // set parameter values
                cmnd.Parameters["@Email"].Value = email;
                cmnd.Parameters["@Pass"].Value = pass;
                cmnd.Parameters["@Temp"].Value = "Normal";
                cmnd.Parameters["@UID"].Value = "-31";
                cmnd.ExecuteNonQuery();

                // read output value 
                Found = Convert.ToInt32(cmnd.Parameters["@Status"].Value); //convert to output parameter to interger format
                UserId = Convert.ToInt32(cmnd.Parameters["@UID"].Value);
                status = Found;
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public int SignMeUp(string name,  string email,string password, string contact)
        {
            int Found = 0;
            Random ActCode = new Random();
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("RegUser ", con); //name of your procedure

                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@Eml", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Pass", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Cntc", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@ActCode", SqlDbType.Int);

                // set parameter values
                Found = ActCode.Next(100000000, 999999999);
                cmnd.Parameters["@Eml"].Value = email;
                cmnd.Parameters["@Pass"].Value = password;
                cmnd.Parameters["@Name"].Value = name;
                cmnd.Parameters["@Cntc"].Value = contact;
                cmnd.Parameters["@ActCode"].Value = Found;
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());
                return -32;
            }
            finally
            {
                con.Close();
            }
            MailActCode(name, email, contact, Found.ToString());
            return Found;
        }

        public void PopulateAlbumList(ref DropDownList item)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT *FROM Albums"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    item.DataSource = cmd.ExecuteReader();
                    item.DataTextField = "AlbumTitle";
                    item.DataValueField = "AlbumId";
                    item.DataBind();
                    con.Close();
                }
            }
            item.Items.Insert(0, new ListItem("--Select Album--", "0"));
        }

        public void PopulatePicsList(string id, ref DropDownList item)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT *FROM Pictures WHERE AlbumID = '"+id+"'"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    item.DataSource = cmd.ExecuteReader();
                    item.DataTextField = "PicTitle";
                    item.DataValueField = "PicID";
                    item.DataBind();
                    con.Close();
                }
            }
            item.Items.Insert(0, new ListItem("--Select Picture--", "0"));
        }

        public void PopulateLanguageList(ref DropDownList item)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT *FROM Languages"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    item.DataSource = cmd.ExecuteReader();
                    item.DataTextField = "LanguageName";
                    item.DataValueField = "LanguageID";
                    item.DataBind();
                    con.Close();
                }
            }
            item.Items.Insert(0, new ListItem("--Select Customer--", "0"));
        }

        public void PopulateMovieList(ref DropDownList item)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT MovieID,Title FROM Movies"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    item.DataSource = cmd.ExecuteReader();
                    item.DataTextField = "Title";
                    item.DataValueField = "MovieID";
                    item.DataBind();
                    con.Close();
                }
            }
            item.Items.Insert(0, new ListItem("--Select Movie--", "0"));
        }

        public void PopulateCastList(ref DropDownList item)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ActorID,ActorName FROM Movies"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    item.DataSource = cmd.ExecuteReader();
                    item.DataTextField = "ActorName";
                    item.DataValueField = "ActorID";
                    item.DataBind();
                    con.Close();
                }
            }
            item.Items.Insert(0, new ListItem("--Select Movie--", "0"));
        }

        public void GetAlbumPics(string AID, ref DataSet DataHolder)
        {
            SqlConnection con = new SqlConnection(connString); //declare and instantiate new SQL connection
            con.Open(); // open sql Connection
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("SELECT * FROM Pictures WHERE AlbumID='"+AID+"'", con);  //instantiate SQL command 
                cmd.CommandType = CommandType.Text; //set type of sqL Command
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(DataHolder); //Add the result  set  returned from SQLCommand to ds
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }

        }

        public DataSet SearchMovie(string SearchString)
        {
            DataSet ds = new DataSet(); //declare and instantiate new dataset
            SqlConnection con = new SqlConnection(connString); //declare and instantiate new SQL connection
            con.Open(); // open sql Connection
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("searchMovie", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@title", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@dateOfRelease", SqlDbType.Date);
                cmd.Parameters.Add("@genreID", SqlDbType.Int);
                cmd.Parameters.Add("@languageIn", SqlDbType.Int);

                cmd.Parameters["@title"].Value = SearchString;
                cmd.Parameters["@dateOfRelease"].Value = "";
                cmd.Parameters["@genreID"].Value = -1;
                cmd.Parameters["@langaugeIn"].Value = -1;

                //  cmd.ExecuteNonQuery();
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds); //Add the result  set  returned from SQLCommand to ds
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }

            return ds; //return the dataset
        }

        public void InsertNewPic(string AlbumID,string PicName, string AddedbyUser)
        {
            SqlConnection con = new SqlConnection(connString); //declare and instantiate new SQL connection
            con.Open(); // open sql Connection
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("AddPicToDB", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@AlID", SqlDbType.Int);
                cmd.Parameters.Add("@PicNam", SqlDbType.VarChar,500);
                cmd.Parameters.Add("@AddBy", SqlDbType.Int);

                cmd.Parameters["@AlID"].Value = Convert.ToInt32(AlbumID);
                cmd.Parameters["@PicNam"].Value = PicName;
                cmd.Parameters["@AddBy"].Value = Convert.ToInt32(AddedbyUser);

                 cmd.ExecuteNonQuery();
                
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }

        }

        public void AddNewAlbum(string AlbumName,string user)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("AddAlbum ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@AlbumName", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@UID", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@AlbumName"].Value = AlbumName;
                cmnd.Parameters["@UID"].Value = user;
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void ActivateUser(string Code)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("ActivateUser", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@ActCode", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@ActCode"].Value = Code;
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public int AddNewCountry(string countryName)
        {
            int Found = -1;
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("addCountry ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@country", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Done", SqlDbType.Int).Direction = ParameterDirection.Output;

                // set parameter values
                cmnd.Parameters["@country"].Value = countryName;
                cmnd.ExecuteNonQuery();

                // read output value 
                Found = Convert.ToInt32(cmnd.Parameters["@Done"].Value); //convert to output parameter to interger format
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
            return Found;
        }

        public int AddNewMovie(string Name,string Genre ,string ReleaseDate,string Language, string MVDesc,string MvGenDesc)
        {
            int Found = -1;
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("AddMovie", con); //name of your procedure
                DateTime YearCheck = Convert.ToDateTime(ReleaseDate);

                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@Genre", SqlDbType.Int);
                cmnd.Parameters.Add("@Status", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmnd.Parameters.Add("@Language", SqlDbType.Int);
                cmnd.Parameters.Add("@MVDesc", SqlDbType.VarChar, 100);
                cmnd.Parameters.Add("@MvGenDesc", SqlDbType.VarChar,50);
                cmnd.Parameters.Add("@ReleaseDate", SqlDbType.Date);

                // set parameter values
                cmnd.Parameters["@Name"].Value = Name;
                cmnd.Parameters["@Genre"].Value = Convert.ToInt32(Genre);
                cmnd.Parameters["@Language"].Value = Convert.ToInt32(Language);
                cmnd.Parameters["@MVDesc"].Value = MVDesc;
                cmnd.Parameters["@MvGenDesc"].Value = MvGenDesc;
                cmnd.Parameters["@ReleaseDate"].Value = Convert.ToDateTime(ReleaseDate);
                cmnd.ExecuteNonQuery();

                // read output value 
                Found = Convert.ToInt32(cmnd.Parameters["@Status"].Value); //convert to output parameter to interger format
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
            return Found;
        }

        public int StartNewPolling (string MovieID)
        {
            int Found = -1;
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("UploadPoll ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@Movie", SqlDbType.Int);
                cmnd.Parameters.Add("@Done", SqlDbType.Int).Direction = ParameterDirection.Output;

                // set parameter values
                cmnd.Parameters["@Movie"].Value = Convert.ToInt32(MovieID);
                cmnd.ExecuteNonQuery();

                // read output value 
                Found = Convert.ToInt32(cmnd.Parameters["@Done"].Value); //convert to output parameter to interger format
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
            return Found;
        }

        public void LanguageEdit(string id, string updt)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("editLanguage ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@lang", SqlDbType.VarChar,50);
                cmnd.Parameters.Add("@id", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@lang"].Value = updt;
                cmnd.Parameters["@id"].Value = Convert.ToInt32(id);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void DelAlbum(string id)
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet PicsList = new DataSet();
            GetAlbumPics(id, ref PicsList);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("DeleteAlbum ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@id", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@id"].Value = Convert.ToInt32(id);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();
                for(int i=0;i<PicsList.Tables[0].Rows.Count;i++)
                {
                    System.IO.File.Delete("/Data/" + PicsList.Tables[0].Rows[i]["PicTitle"].ToString());
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void DelPic(string id)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("DeletePic ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@PicID", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@PicID"].Value = Convert.ToInt32(id);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void UpdateGenre(string id, string GenreTtl)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("editGenre ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@type", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@id", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@type"].Value = GenreTtl;
                cmnd.Parameters["@id"].Value = Convert.ToInt32(id);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void EditCountry(string id, string updt)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("editCountry ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@country", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@id", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@country"].Value = updt;
                cmnd.Parameters["@id"].Value = Convert.ToInt32(id);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void DelCountry(string id)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("DelCountry ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@id", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@id"].Value = Convert.ToInt32(id);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void AddCastToMovie(string sctid, string mid)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("UserLogin ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@act", SqlDbType.Int);
                cmnd.Parameters.Add("@mid", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@act"].Value = Convert.ToInt32(sctid);
                cmnd.Parameters["@mid"].Value = Convert.ToInt32(mid);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public void AddActors(string name, string cntID)
        {
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("AddAct ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@ActName", SqlDbType.VarChar, 50);
                cmnd.Parameters.Add("@CntId", SqlDbType.Int);

                // set parameter values
                cmnd.Parameters["@ActName"].Value = name;
                cmnd.Parameters["@CntId"].Value = Convert.ToInt32(cntID);
                cmnd.ExecuteNonQuery();

                // read output value 
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }
        }

        public string GetUserName(int id)
        {
            string link="";
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("GetUserName ", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@UsrID", SqlDbType.Int);
                cmnd.Parameters.Add("@Name", SqlDbType.VarChar,500).Direction = ParameterDirection.Output;

                // set parameter values
                cmnd.Parameters["@UsrID"].Value = id;
                cmnd.ExecuteNonQuery();

                // read output value 
                link = cmnd.Parameters["@Name"].Value.ToString(); //convert to output parameter to interger format
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }

            return link;
        }

        public string GetAlbumName(int id)
        {
            string link = "";
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            try
            {
                SqlCommand cmnd = new SqlCommand("GetAlbumTitle", con); //name of your procedure
                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.Add("@AlID", SqlDbType.Int);
                cmnd.Parameters.Add("@AlTtl", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;

                // set parameter values
                cmnd.Parameters["@AlID"].Value = id;
                cmnd.ExecuteNonQuery();

                // read output value 
                link = cmnd.Parameters["@AlTtl"].Value.ToString(); //convert to output parameter to interger format
                con.Close();


            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());

            }
            finally
            {
                con.Close();
            }

            return link;
        }

        public void SubmitRating(string id,string mid)
        {
            SqlConnection con = new SqlConnection(connString); //declare and instantiate new SQL connection
            con.Open(); // open sql Connection
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("INSERT INTO RatingByNonRegUsers VALUES('" + id + "', '"+mid+"'", con);  //instantiate SQL command 
                cmd.CommandType = CommandType.Text; //set type of sqL Command
                cmd.ExecuteReader();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("SQL Error" + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }
        }

        public void MailActCode(string name, string Email, string contactNo, string ActCode)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add("danialves.chelsea@gmail.com");
            mail.From = new MailAddress("danialves.chelsea@gmail.com", "FamilyDiv", System.Text.Encoding.UTF8);
            mail.Subject = "Activation Code For FamilyDiv For Email '" + Email + "'";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Hey " + name + ", Welcome to FamilyDiv. Please type in the activation code while loging in on our website for the first time. Your activation code is : " + ActCode + ". Thankyou for registering at our webiste. We wish you a happy day.";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("danialves.chelsea@gmail.com", "mov ax, bx");
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            try
            {
                client.Send(mail);
                //Page.RegisterStartupScript("UserMsg", "<script>alert('Successfully Send...');if(alert){ window.location='SendMail.aspx';}</script>");
            }
            catch (Exception ex)
            {
                Exception ex2 = ex;
                string errorMessage = string.Empty;
                while (ex2 != null)
                {
                    errorMessage += ex2.ToString();
                    ex2 = ex2.InnerException;
                }
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Email can't be sent at the moment')", true);
                //Page.RegisterStartupScript("UserMsg", "<script>alert('Sending Failed...');if(alert){ window.location='SendMail.aspx';}</script>");
            }
        }
    }
}