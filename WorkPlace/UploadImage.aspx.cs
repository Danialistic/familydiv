﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Faizi.DAL;

namespace Faizi
{
    public partial class UploadImage : System.Web.UI.Page
    {
        myDAL Connection = new myDAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                Boolean fileOK = false;
                String path = Server.MapPath("~/Data/");
                if (FileUpload1.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(this.ImgName.Text.ToString()))
                        {
                            FileUpload1.PostedFile.SaveAs(path
                            + FileUpload1.FileName);
                            Connection.InsertNewPic(Session["SelAlbumID"].ToString(), FileUpload1.FileName, Session["LoggedInUserID"].ToString());
                        }
                        else
                        {
                            int temp = 0;
                            bool saga=false;
                            string FinalName = "";

                            for (; !saga;temp++ )
                            {
                                if (FileUpload1.FileName[temp] == '.')
                                    saga = true;
                            }
                            FinalName = ImgName.Text.ToString() + '.' + FileUpload1.FileName.Substring(temp, FileUpload1.FileName.Length - temp);
                                FileUpload1.PostedFile.SaveAs(path
                                + ImgName.Text.ToString() + '.' + FileUpload1.FileName.Substring(temp,FileUpload1.FileName.Length-temp));
                                Connection.InsertNewPic(Session["SelAlbumID"].ToString(), FinalName , Session["LoggedInUserID"].ToString());
                        }
                        this.RespnseHandlerLabel.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        this.RespnseHandlerLabel.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    this.RespnseHandlerLabel.Text = "Cannot accept files of this type.";
                }
            }

            Response.Redirect("PicViewPage.aspx");
        }
    }
}