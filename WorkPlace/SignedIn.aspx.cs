﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Faizi.DAL;

namespace Faizi
{
    public partial class SignedIn : System.Web.UI.Page
    {
        myDAL DBConnection = new myDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UName.Text = DBConnection.GetUserName(Convert.ToInt32(Session["LoggedInUserID"].ToString()));
            MainHead.Text = "Welcome To FamilyDiv," + " " + this.UName.Text;
        }

        protected void VA_Click(object sender, EventArgs e)
        {
            this.MainMenu.Visible = false;
            this.SelAlPan.Visible = true;
            DBConnection.PopulateAlbumList(ref this.Albums);
        }

        protected void GoView_Click(object sender, EventArgs e)
        {
            Session["SelAlbumID"] = this.Albums.SelectedValue.ToString();
            Response.Redirect("PicViewPage.aspx");
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.SelAlPan.Visible = false;
            this.MainMenu.Visible = true;
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.AlbumName.Text.ToString()))
            {
                DBConnection.AddNewAlbum(this.AlbumName.Text.ToString(), Session["LoggedInUserID"].ToString());
                this.CrAlPan.Visible = false;
                this.MainMenu.Visible = true;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter an album name first!')", true);
            }
        }

        protected void CancelCr_Click(object sender, EventArgs e)
        {
            this.AlbumName.Text = ".";
            this.CrAlPan.Visible = false;
            this.MainMenu.Visible = true;
        }

        protected void CA_Click(object sender, EventArgs e)
        {
            this.MainMenu.Visible = false;
            this.CrAlPan.Visible = true;
        }

        protected void DelAl_Click(object sender, EventArgs e)
        {
            if(this.Albums.SelectedIndex>0)
            {
                this.DBConnection.DelAlbum(this.Albums.SelectedValue.ToString());
                Response.Redirect("SignedIn.aspx");
            }
        }
    }
}